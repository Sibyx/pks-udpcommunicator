# PcapAnalyzer

Implementacia C++ programu na zarucene prenasanie sprav nad UDP protokolom.

## Kompilacia a vyvojove prostredie

```
mkdir build
cd build

cmake --target UdpCommunicator -DCMAKE_BUILD_TYPE=Release ..
make
```

Vysledok je binarny subor `UdpCommunicator`.

## Kniznice

- [CLI11](https://github.com/CLIUtils/CLI11): Kniznica pre jednoduchsie nacitavanie CLI parametrov.
- [CRCpp](https://github.com/d-bahr/CRCpp): Implementacia CRC algoritmov v C++ 

## Zdroje

- [How to convert string to IP address and vice versa](https://stackoverflow.com/questions/5328070/how-to-convert-string-to-ip-address-and-vice-versa): Konverzia IP adries z retazca a naspat

---
Jakub Dubec (c) 2018-2019