//
// Created by Jakub Dubec on 18/11/2018.
//

#ifndef UDPCOMMUNICATOR_FRAMETYPE_H
#define UDPCOMMUNICATOR_FRAMETYPE_H

enum TransportType { messageTransport, fileTransport };
enum FrameType { newMessageType, dataType, ackType, nackType, aliveType };

#endif //UDPCOMMUNICATOR_FRAMETYPE_H
