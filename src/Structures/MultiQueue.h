//
// Created by Jakub Dubec on 18/11/2018.
//

#ifndef UDPCOMMUNICATOR_MULTIQUEUE_H
#define UDPCOMMUNICATOR_MULTIQUEUE_H



#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

template <typename T> class MultiQueue {
public:
	T pop() {
		std::unique_lock<std::mutex> mlock(mutex_);
		while (queue_.empty()) {
			cond_.wait(mlock);
		}
		auto val = queue_.front();
		queue_.pop();
		return val;
	}

	void pop(T& item) {
		std::unique_lock<std::mutex> mlock(mutex_);
		while (queue_.empty()) {
			cond_.wait(mlock);
		}
		item = queue_.front();
		queue_.pop();
	}

	void push(const T& item) {
		std::unique_lock<std::mutex> mlock(mutex_);
		queue_.push(item);
		mlock.unlock();
		cond_.notify_one();
	}

	void clear() {
		std::unique_lock<std::mutex> mlock(mutex_);
		std::queue<T> empty;
		std::swap(queue_, empty);
		mlock.unlock();
		cond_.notify_one();
	}

	unsigned long size() {
		std::unique_lock<std::mutex> mlock(mutex_);
		return queue_.size();
	}

	MultiQueue() = default;
	MultiQueue(const MultiQueue&) = delete;
	MultiQueue& operator=(const MultiQueue&) = delete;

private:
	std::queue<T> queue_;
	std::mutex mutex_;
	std::condition_variable cond_;
};


#endif //UDPCOMMUNICATOR_MULTIQUEUE_H
