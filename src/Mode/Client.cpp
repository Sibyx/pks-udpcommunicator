//
// Created by Jakub Dubec on 11/11/2018.
// https://www.geeksforgeeks.org/udp-server-client-implementation-c/
//

#include <iostream>
#include <netdb.h>
#include <thread>
#include <fstream>
#include <signal.h>
#include "Client.h"
#include "../Frames/NewMessage.h"
#include "../Frames/KeepAlive.h"
#include "../Frames/Data.h"

Client::Client(std::string ip, int port, int fragmentSize, int framesToDestroy, int paritySkip) {
	this->fragmentSize = fragmentSize;
	this->framesToDestroy = framesToDestroy;
	this->paritySkip = paritySkip;
	this->sock = socket(AF_INET, SOCK_DGRAM, 0);

	this->ioMutex.lock();
	std::cout << "Frames to destroy: " << this->framesToDestroy << std::endl;
	this->ioMutex.unlock();

	if (this->sock < 0)
		throw std::runtime_error("Unable to open client socket!");

	// Create server address
	this->serverAddress = (sockaddr_in*) malloc(sizeof(sockaddr_in));
	this->serverAddress->sin_family = AF_INET; // IPv4
	inet_pton(AF_INET, ip.c_str(), &this->serverAddress->sin_addr.s_addr); // string to IP
	this->serverAddress->sin_port = (in_port_t) htons(port); //port

	struct timeval timeout{100, 0};

	if (setsockopt(this->sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
		throw std::runtime_error("Setsockopt failed! (Tried to set SOL_SOCKET & SO_RCVTIMEO)");

	std::thread keepAliveThread(&Client::keepAlive, this);
	std::thread receiverThread(&Client::receiver, this);

	this->cli();
}

int Client::cli() {
	std::string input;

	this->ioMutex.lock();
	std::cout << "What would you like to do? (F - File, M - Message, E - Exit): ";
	getline(std::cin, input);
	this->ioMutex.unlock();

	switch (input[0]) {
		case 'F':
			this->prepareFile();
			break;
		case 'M':
			this->prepareString();
			break;
		case 'E':
			std::cout << "See you bitch!" << std::endl;
			close(this->sock);
			exit(0);
		default:
			std::cerr << "Invalid input!" << std::endl;
			return this->cli();
	}

	this->isSending.store(true);

	std::thread senderThread(&Client::sender, this);

	senderThread.join();

	return this->cli();
}

void Client::receiver() {
	struct sockaddr_in clientAddress;
	int clientAddressLength = sizeof(clientAddress);

	char *buffer = (char*) malloc(BUFFER_SIZE * sizeof(char));

	while (recvfrom(this->sock, buffer, BUFFER_SIZE, 0, (sockaddr*) &clientAddress, (socklen_t*) &clientAddressLength)) {
		switch (buffer[0]) {
			case FrameType::ackType:
				this->processAck(new Ack(buffer));
				break;
			case FrameType::nackType:
				this->processNack(new Nack(buffer));
				break;
			default:
				std::cerr << "Received invalid header type!" << std::endl;
				break;
		}
	}

	this->ioMutex.lock();
	std::cout << "Threads: Finished receiving." << std::endl;
	this->ioMutex.unlock();

	exit(1);
}

void Client::sender() {
	AbstractFrame *frame;

	while (this->isSending.load()) {
		while (this->sendQueue.size()) {
			frame = this->sendQueue.pop();
			this->ioMutex.lock();
			std::cout << "Sending: " << frame->toString() << std::endl;
			this->ioMutex.unlock();

			if ((this->framesToDestroy > 0) && (frame->getFrame() == FrameType::dataType)) {
				auto dataFrame = new Data(frame->toBinary());

				this->ioMutex.lock();
				std::cout << "Corrupting: " << dataFrame->toString() << std::endl;
				this->ioMutex.unlock();

				dataFrame->destroyData();
				frame = dataFrame;

				this->framesToDestroy--;
			}

			sendto(this->sock, frame->toBinary(), frame->size(), 0, (const struct sockaddr *) this->serverAddress, sizeof(*this->serverAddress));
			usleep(100);
		}
	}

	this->ioMutex.lock();
	std::cout << "Threads: Finished sending." << std::endl;
	this->ioMutex.unlock();
}

void Client::keepAlive() {
	while (true) {
		sleep(40);

		auto *alive = new KeepAlive();

		this->ioMutex.lock();
		std::cout << "Sending: " << alive->toString() << std::endl;
		this->ioMutex.unlock();

		if (!sendto(this->sock, alive->toBinary(), alive->size(), 0, (const struct sockaddr *) this->serverAddress, sizeof(*this->serverAddress)))
			throw std::runtime_error("Unable to send KeepAlive!");

		free(alive);
	}
}

void Client::processAck(Ack *ack) {
	this->ioMutex.lock();
	std::cout << "Receiving: " << ack->toString() << std::endl;
	this->ioMutex.unlock();

	if ((ack->getFragment() == 0)) {
		this->isSending.store(true);
		for (const auto& item : this->preparedData) {
			if (item.second->getFrame() != FrameType::newMessageType) {
				this->sendQueue.push(item.second);
			}
		}
	} else if ((ack->getFragment() == (this->preparedData.size() - 1)) && this->isSending) {
		this->sendQueue.clear();
		this->preparedData.clear();
		this->isSending.store(false);

		this->ioMutex.lock();
		std::cout << "Message processed!" << std::endl;
		this->ioMutex.unlock();
	}
}

void Client::processNack(Nack *nack) {
	this->ioMutex.lock();
	std::cout << "Receiving: " << nack->toString() << std::endl;
	this->ioMutex.unlock();

	if (nack->getFragment() > 0) {
		this->sendQueue.push(this->preparedData[nack->getFragment()]);
	}
}

void Client::prepareFile() {
	size_t bufferSize = this->fragmentSize - 7 * sizeof(char);
	size_t numberOfFragments = 0;
	char *buffer = (char*) malloc(bufferSize);
	std::ifstream myfile;
	std::string filename;
	int fragmentIndex = 0;

	this->ioMutex.lock();
	std::cout << "File name to  send: ";
	getline(std::cin, filename);
	this->ioMutex.unlock();

	myfile.open(filename, std::ios::binary | std::ios::in);

	if (myfile.is_open()) {
		while (myfile.read(buffer, bufferSize)) {
			numberOfFragments++;
			std::streamsize s = myfile.gcount();
			this->preparedData[numberOfFragments] = new Data(static_cast<int>(numberOfFragments), static_cast<int>(s), buffer);
		}
	} else {
		std::cerr << "File not found!" << std::endl;
		return;
	}
	myfile.close();

	this->preparedData[0] = new NewMessage(TransportType::fileTransport, static_cast<int>(numberOfFragments));
	this->sendQueue.push(this->preparedData[0]);
	this->sendQueue.push(this->preparedData[0]);
	this->sendQueue.push(this->preparedData[0]);
	this->sendQueue.push(this->preparedData[0]);
}

void Client::prepareString() {
	size_t chunkSize = this->fragmentSize - 7 * sizeof(char);
	size_t numberOfFragments = 0;
	int fragmentIndex = 0;
	std::string message;

	this->ioMutex.lock();
	std::cout << "Message to send: ";
	getline(std::cin, message);
	this->ioMutex.unlock();

	std::cout << "Chunk size: "<< chunkSize << std::endl;

	for (unsigned i = 0; i < message.length(); i += chunkSize) {
		fragmentIndex++;
		if (this->paritySkip) {
			if ((fragmentIndex % 2) && (fragmentIndex != 0)) {
				numberOfFragments++;
				std::string chunk = message.substr(i, chunkSize);
				this->preparedData[numberOfFragments] = new Data(static_cast<int>(numberOfFragments), static_cast<int>(chunk.size()), (char *) chunk.c_str());
			}
		}
		else {
			numberOfFragments++;
			std::string chunk = message.substr(i, chunkSize);
			this->preparedData[numberOfFragments] = new Data(static_cast<int>(numberOfFragments), static_cast<int>(chunk.size()), (char *) chunk.c_str());
		}
	}

	this->preparedData[0] = new NewMessage(TransportType::messageTransport, static_cast<int>(numberOfFragments));
	this->sendQueue.push(this->preparedData[0]);
	this->sendQueue.push(this->preparedData[0]);
	this->sendQueue.push(this->preparedData[0]);
	this->sendQueue.push(this->preparedData[0]);

}
