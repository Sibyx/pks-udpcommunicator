//
// Created by Jakub Dubec on 11/11/2018.
//

#include <netdb.h>
#include <iostream>
#include <thread>
#include <fstream>
#include <sstream>
#include "Server.h"
#include "../Frames/NewMessage.h"
#include "../Frames/Nack.h"

Server::Server(const std::string ip, const int port) {
	std::printf("Starting server on %s:%d\n", ip.c_str(), port);

	this->sock = socket(AF_INET, SOCK_DGRAM, 0);

	if (this->sock < 0)
		throw std::runtime_error("Unable to open server socket!");

	// Resolve already in use debbuging problem
	this->optval = 1;
	setsockopt(this->sock, SOL_SOCKET, SO_REUSEADDR, &this->optval, sizeof(int));

	// Create server address
	this->serverAddress = (sockaddr_in*) malloc(sizeof(sockaddr_in));
	this->serverAddress->sin_family = AF_INET; // IPv4
	inet_pton(AF_INET, ip.c_str(), &this->serverAddress->sin_addr.s_addr); // string to IP
	this->serverAddress->sin_port = (in_port_t) htons(port);

	std::thread senderThread(&Server::sender, this);
	senderThread.detach();

	this->receiver();
}

void Server::receiver() {
	if (bind(this->sock, (struct sockaddr *) this->serverAddress, sizeof(sockaddr_in)) < 0)
		throw std::runtime_error("Can't bind to selected port!");

	struct sockaddr_in clientAddress{};
	socklen_t clientAddressLength = sizeof(clientAddress);
	ssize_t messageSize;

	while (true) {
		char *buffer = (char*) malloc(BUFFER_SIZE * sizeof(char));
		messageSize = recvfrom(this->sock, buffer, BUFFER_SIZE, 0, (sockaddr*) &clientAddress, &clientAddressLength);

		if (messageSize < 0)
			throw std::runtime_error("Unable to receive message.");

		switch (buffer[0]) {
			case FrameType::newMessageType:
				this->processNewMessage(new NewMessage(buffer), clientAddress, clientAddressLength);
				break;
			case FrameType::aliveType:
				this->processKeepAlive(new KeepAlive(buffer));
				break;
			case FrameType::dataType:
				this->processData(new Data(buffer));
				break;
			default:
				std::cerr << "Received invalid header type!" << std::endl;
				break;
		}
	}
}

void Server::processNewMessage(NewMessage *message, sockaddr_in clientAddress, socklen_t clientAddressLength) {
	if (this->isReceiving.load())
		return;

	this->ioMutex.lock();
	std::cout << "Receiving: " <<  message->toString() << std::endl;
	this->ioMutex.unlock();

	this->senderAddress = clientAddress;
	this->senderAddressLength = clientAddressLength;

	this->receivedData[0] = message;
	this->isReceiving.store(true);

	this->sendQueue.push(new Ack(0));

	std::thread requestThread(&Server::requestMissing, this);

	requestThread.detach();
}

void Server::processKeepAlive(KeepAlive *alive) {
	this->ioMutex.lock();
	std::cout << "Receiving: " << alive->toString() << std::endl;
	this->ioMutex.unlock();
	this->sendQueue.push(new Ack(-1));
}

void Server::processData(Data *data) {
	if (!isReceiving.load())
		return;

	this->ioMutex.lock();
	std::cout << "Receiving: " << data->toString() << std::endl;
	this->ioMutex.unlock();

	this->lastReceiveAt.store(this->getCurrentTime());

	if (data->isValid()) {
		if (!this->receivedData[data->getFragment()]) {
			this->receivedData[data->getFragment()] = data;
		}
	} else {
		this->sendQueue.push(new Nack(data->getFragment()));
	}

	NewMessage *newMessage = dynamic_cast<NewMessage *>(this->receivedData[0]);

	if (this->receivedData.size() == newMessage->getFragments() + 1) {
		this->finishTransfer(newMessage);
	}
}

time_t Server::getCurrentTime() {
	auto now = std::chrono::system_clock::now();
	return std::chrono::system_clock::to_time_t(now);
}

void Server::sender() {
	AbstractFrame *frame;

	while ((frame = this->sendQueue.pop())) {
		this->ioMutex.lock();
		std::cout << "Sending: " << frame->toString() << std::endl;
		this->ioMutex.unlock();
		sendto(this->sock, frame->toBinary(), frame->size(), 0, (const struct sockaddr *) &this->senderAddress, this->senderAddressLength);
		usleep(100);
	}

	std::cout << "Threads: Ending senderThread" << std::endl;
}

void Server::requestMissing() {
	sleep(5);
	while (this->isReceiving.load()) {
		if ((this->getCurrentTime() - this->lastReceiveAt.load()) > 3) {
			NewMessage *newMessage = dynamic_cast<NewMessage *>(this->receivedData[0]);

			for (int i = 1; i <= newMessage->getFragments(); ++i) {
				if (!this->receivedData.count(i)) {
					this->sendQueue.push(new Nack(i));
				}
			}
		}
	}

	this->ioMutex.lock();
	std::cout << "Threads: Ending requestMissingThread" << std::endl;
	this->ioMutex.unlock();
}

void Server::finishTransfer(NewMessage *newMessage) {
	Data *myData;

	this->sendQueue.clear();
	this->isReceiving.store(false);


	auto ack = new Ack(newMessage->getFragments());
	sendto(this->sock, ack->toBinary(), ack->size(), 0, (const struct sockaddr *) &this->senderAddress, this->senderAddressLength);
	sendto(this->sock, ack->toBinary(), ack->size(), 0, (const struct sockaddr *) &this->senderAddress, this->senderAddressLength);
	sendto(this->sock, ack->toBinary(), ack->size(), 0, (const struct sockaddr *) &this->senderAddress, this->senderAddressLength);
	sendto(this->sock, ack->toBinary(), ack->size(), 0, (const struct sockaddr *) &this->senderAddress, this->senderAddressLength);

	if (newMessage->getTransportType() == TransportType::fileTransport) {
		std::string fileName;
		std::ofstream myfile;

		this->ioMutex.lock();
		std::cout << "Hi buddy! Tell us your file name: ";
		getline(std::cin, fileName);
		this->ioMutex.unlock();

		myfile.open(fileName, std::ios::out | std::ios::binary);
		if (myfile.is_open()) {
			for (const auto& item : this->receivedData) {
				if (item.second->getFrame() != FrameType::newMessageType) {
					myData = dynamic_cast<Data *>(item.second);
					myfile.write(myData->getContent(), myData->getLength());
				}
			}
		}
		myfile.close();
	}
	else if (newMessage->getTransportType() == TransportType::messageTransport) {
		this->ioMutex.lock();
		std::cout << "Hi buddy! I am just received this message: ";

		for (const auto& item : this->receivedData) {
			if (item.second->getFrame() != FrameType::newMessageType) {
				myData = dynamic_cast<Data *>(	item.second);
				std::cout << myData->getContent();
			}
			std::cout << std::endl;
		}
		this->ioMutex.unlock();
	} else {
		std::cerr << "Received invalid transport type!" << std::endl;
	}

	this->receivedData.clear();

	this->ioMutex.lock();
	std::cout << "All data received and collected!" << std::endl;
	this->ioMutex.unlock();
}
