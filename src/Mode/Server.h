//
// Created by Jakub Dubec on 11/11/2018.
//

#ifndef UDPCOMMUNICATOR_SERVER_H
#define UDPCOMMUNICATOR_SERVER_H


#include <string>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <map>
#include <unistd.h>
#include "../Frames/NewMessage.h"
#include "../Frames/Ack.h"
#include "../Frames/KeepAlive.h"
#include "../Frames/Data.h"
#include "../Structures/MultiQueue.h"

#define BUFFER_SIZE 1024

class Server {
private:
	std::mutex ioMutex;
	int sock;
	sockaddr_in *serverAddress;
	int optval;
	std::map<int, AbstractFrame*> receivedData;
	MultiQueue<AbstractFrame*> sendQueue;
	std::atomic<bool> isReceiving;
	std::atomic<time_t> lastReceiveAt;
	struct sockaddr_in senderAddress;
	socklen_t senderAddressLength = sizeof(senderAddress);

	time_t getCurrentTime();
	void processNewMessage(NewMessage *message, sockaddr_in clientAddress, socklen_t clientAddressLength);
	void processKeepAlive(KeepAlive *alive);
	void processData(Data *data);
	void receiver();
	void sender();
	void requestMissing();
	void finishTransfer(NewMessage *newMessage);
public:
	Server(std::string ip, int port);
};


#endif //UDPCOMMUNICATOR_SERVER_H
