//
// Created by Jakub Dubec on 11/11/2018.
//

#ifndef UDPCOMMUNICATOR_CLIENT_H
#define UDPCOMMUNICATOR_CLIENT_H


#include <string>
#include <map>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include "../Frames/Data.h"
#include "../Structures/MultiQueue.h"
#include "../Frames/Ack.h"
#include "../Frames/Nack.h"

#define BUFFER_SIZE 1024

class Client {
private:
	std::mutex ioMutex;
	sockaddr_in *serverAddress;
	int sock;
	int fragmentSize;
	int framesToDestroy;
	int paritySkip;
	std::map<int, AbstractFrame*> preparedData;
	MultiQueue<AbstractFrame*> sendQueue;
	std::atomic<bool> isSending;
	int cli();
	void receiver();
	void sender();
	void keepAlive();
	void prepareFile();
	void prepareString();
	void processAck(Ack *ack);
	void processNack(Nack *nack);
public:
	Client(std::string ip, int port, int fragmentSize, int framesToDestroy, int paritySkip);
};


#endif //UDPCOMMUNICATOR_CLIENT_H
