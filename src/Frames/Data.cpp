//
// Created by Jakub Dubec on 18/11/2018.
//

#include "Data.h"

#define CRC16 0x8005

Data::Data(char *data) {
	int *int_pointer;
	// Read frame
	// TODO: runtime exception ak zly contex
	this->frame = data[0];

	// Read fragments
	int_pointer = (int*) (data + 1);
	this->fragment = *int_pointer;

	// Read length
	int_pointer = (int*) (data + 1 + sizeof(int));
	this->length = static_cast<size_t>(*int_pointer);

	// Read content
	this->content = (char *) malloc(this->length * sizeof(char));
	memcpy(this->content, data + 2 * sizeof(int) + 1, this->length);

	// Read checksum
	int_pointer = (int*) (data + 2 * sizeof(int) + 1 + this->length);
	this->checksum = *int_pointer;
}

Data::Data(int fragment, int length, char *data) {
	this->frame = FrameType::dataType;
	this->fragment = fragment;
	this->length = static_cast<size_t>(length);
	this->content = (char *) malloc(this->length * sizeof(char));

	memcpy(this->content, data, this->length);
	this->checksum = this->createCRC(this->content, this->length);
}

char *Data::toBinary() {
	char *data = (char*) malloc(this->size());
	int *intprt = (int *) (data + 1);

	// Frame type
	data[0] = this->frame;

	// Fragment number
	*intprt = this->fragment;

	// Payload length
	intprt = (int *) (data + 1 + sizeof(int));
	*intprt = static_cast<int>(this->length);

	// Copy content
	memcpy(data + 1 + sizeof(int) * 2, this->content, this->length);

	intprt = (int *) (data + 1 + sizeof(int) * 2 + this->length);
	*intprt = this->checksum;

	return data;
}

size_t Data::size() {
	return sizeof(char) + sizeof(int) + sizeof(int) + sizeof(int) + this->length;
}

std::string Data::toString() {
	char *buffer = (char*) malloc(100);
	sprintf(buffer, "Data(frame, fragment, length, checksum, isValid) {%d, %d, %d, %d, %d}", this->frame, this->fragment, static_cast<int>(this->length), this->checksum, this->isValid());
	return std::string(buffer);
}

int Data::createCRC(char *data, size_t size) {
	uint16_t out = 0;
	int bits_read = 0, bit_flag;

	/* Sanity check: */
	if(data == nullptr)
		return 0;

	while (size > 0) {
		bit_flag = out >> 15;

		/* Get next bit: */
		out <<= 1;
		out |= (*data >> bits_read) & 1; // item a) work from the least significant bits

		/* Increment bit counter: */
		bits_read++;
		if(bits_read > 7) {
			bits_read = 0;
			data++;
			size--;
		}

		/* Cycle check: */
		if (bit_flag)
			out ^= CRC16;
	}

	// item b) "push out" the last 16 bits
	int i;
	for (i = 0; i < 16; ++i) {
		bit_flag = out >> 15;
		out <<= 1;
		if(bit_flag)
			out ^= CRC16;
	}

	// item c) reverse the bits
	uint16_t crc = 0;
	i = 0x8000;
	int j = 0x0001;
	for (; i != 0; i >>=1, j <<= 1) {
		if (i & out) crc |= j;
	}

	return crc;
}

bool Data::isValid() {
	return (this->checksum == this->createCRC(this->content, this->length));
}

int Data::getFragment() {
	return this->fragment;
}

size_t Data::getLength() {
	return this->length;
}

char *Data::getContent() {
	return this->content;
}

void Data::destroyData() {
	for (int i = 0; i < this->getLength(); ++i) {
		if (!(i % 5)) {
			this->content[i] = 42;
		}
	}
}
