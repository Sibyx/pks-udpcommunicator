//
// Created by Jakub Dubec on 18/11/2018.
//

#include "Nack.h"
#include "../Structures/Enums.h"

Nack::Nack(char *data) {
	int *intprt;

	// Read frame
	// TODO: runtime exception ak zly contex
	this->frame = data[0];

	// Read fragment
	intprt = (int*) (data + 1);
	this->fragment = *intprt;
}

Nack::Nack(int fragment) {
	this->frame = FrameType::nackType;
	this->fragment = fragment;
}

char *Nack::toBinary() {
	char *data = (char*) malloc(this->size());
	int *intprt = (int *) (data + 1);

	data[0] = this->frame;
	*intprt = this->fragment;

	return data;
}

size_t Nack::size() {
	return sizeof(char) + sizeof(int);
}

std::string Nack::toString() {
	char buffer[100];
	sprintf(buffer, "Nack(frame, fragment) {%d, %d}", this->frame, this->fragment);
	return std::string(buffer);
}

int Nack::getFragment() {
	return this->fragment;
}
