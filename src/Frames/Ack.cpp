//
// Created by Jakub Dubec on 18/11/2018.
//

#include "Ack.h"
#include "../Structures/Enums.h"

Ack::Ack(char *data) {
	int *intprt;

	// Read frame
	// TODO: runtime exception ak zly contex
	this->frame = data[0];

	// Read fragments
	intprt = (int*) (data + 1);
	this->fragment = *intprt;
}

Ack::Ack(int fragment) {
	this->frame = FrameType::ackType;
	this->fragment = fragment;
}

char *Ack::toBinary() {
	char *data = (char*) malloc(this->size());
	int *intprt = (int *) (data + 1);

	data[0] = this->frame;
	*intprt = this->fragment;

	return data;
}

size_t Ack::size() {
	return sizeof(char) + sizeof(int);
}

std::string Ack::toString() {
	char buffer[100];
	sprintf(buffer, "Ack(frame, fragment) {%d, %d}", this->frame, this->fragment);
	return std::string(buffer);
}

int Ack::getFragment() {
	return this->fragment;
}
