//
// Created by Jakub Dubec on 18/11/2018.
//

#ifndef UDPCOMMUNICATOR_ACK_H
#define UDPCOMMUNICATOR_ACK_H


#include <string>
#include "AbstractFrame.h"

class Ack : public AbstractFrame {
public:
	explicit Ack(char *data);
	explicit Ack(int fragment);
	char* toBinary() override;
	size_t size() override;
	std::string toString() override;
	int getFragment();
private:
	int fragment;
};


#endif //UDPCOMMUNICATOR_ACK_H
