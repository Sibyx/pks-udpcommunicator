//
// Created by Jakub Dubec on 18/11/2018.
//

#include "KeepAlive.h"
#include "../Structures/Enums.h"

KeepAlive::KeepAlive(char *data) {
	// Read frame
	// TODO: runtime exception ak zly contex
	this->frame = data[0];
}

KeepAlive::KeepAlive() {
	this->frame = FrameType::aliveType;
}

char *KeepAlive::toBinary() {
	char *data = (char*) malloc(this->size());
	data[0] = this->frame;
	return data;
}

size_t KeepAlive::size() {
	return sizeof(char);
}

std::string KeepAlive::toString() {
	char buffer[100];
	sprintf(buffer, "KeepAlive(frame) {%d}", this->frame);
	return std::string(buffer);
}
