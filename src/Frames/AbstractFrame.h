//
// Created by Jakub Dubec on 18/11/2018.
//

#ifndef UDPCOMMUNICATOR_ABSTRACTFRAME_H
#define UDPCOMMUNICATOR_ABSTRACTFRAME_H


#include <string>
#include "../Structures/Enums.h"

class AbstractFrame {
public:
	virtual char* toBinary() = 0;
	virtual size_t size() = 0;
	virtual std::string toString() = 0;
	FrameType getFrame();
protected:
	char frame;
};


#endif //UDPCOMMUNICATOR_ABSTRACTFRAME_H
