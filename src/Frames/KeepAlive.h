//
// Created by Jakub Dubec on 18/11/2018.
//

#ifndef UDPCOMMUNICATOR_KEEPALIVE_H
#define UDPCOMMUNICATOR_KEEPALIVE_H


#include <string>
#include "AbstractFrame.h"

class KeepAlive : public AbstractFrame {
public:
	explicit KeepAlive(char *data);
	explicit KeepAlive();
	char* toBinary() override;
	size_t size() override;
	std::string toString() override;
};


#endif //UDPCOMMUNICATOR_KEEPALIVE_H
