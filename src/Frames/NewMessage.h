//
// Created by Jakub Dubec on 18/11/2018.
//

#ifndef UDPCOMMUNICATOR_NEWMESSAGE_H
#define UDPCOMMUNICATOR_NEWMESSAGE_H


#include <string>
#include "../Structures/Enums.h"
#include "AbstractFrame.h"

class NewMessage : public AbstractFrame {
public:
	explicit NewMessage(char *data);
	NewMessage(TransportType type, int fragments);
	char* toBinary() override;
	size_t size() override;
	std::string toString() override;
	int getFragments();
	char getTransportType();
private:
	char type;
	int fragments;
};


#endif //UDPCOMMUNICATOR_NEWMESSAGE_H
