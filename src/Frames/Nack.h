//
// Created by Jakub Dubec on 18/11/2018.
//

#ifndef UDPCOMMUNICATOR_NACK_H
#define UDPCOMMUNICATOR_NACK_H


#include <string>
#include "AbstractFrame.h"

class Nack : public AbstractFrame {
public:
	explicit Nack(char *data);
	explicit Nack(int fragment);
	char* toBinary() override;
	size_t size() override;
	std::string toString() override;
	int getFragment();
private:
	int fragment;
};


#endif //UDPCOMMUNICATOR_NACK_H
