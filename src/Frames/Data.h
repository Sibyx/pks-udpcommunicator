//
// Created by Jakub Dubec on 18/11/2018.
//

#ifndef UDPCOMMUNICATOR_DATA_H
#define UDPCOMMUNICATOR_DATA_H


#include <string>
#include "../Structures/Enums.h"
#include "AbstractFrame.h"

class Data : public AbstractFrame {
public:
	explicit Data(char *data);
	Data(int fragment, int length, char *data);
	char* toBinary() override;
	size_t size() override;
	std::string toString() override;
	bool isValid();
	int getFragment();
	size_t getLength();
	char *getContent();
	void destroyData();
private:
	int fragment;
	size_t length;
	char *content;
	int checksum;
	int createCRC(char *data, size_t size);
};


#endif //UDPCOMMUNICATOR_DATA_H
