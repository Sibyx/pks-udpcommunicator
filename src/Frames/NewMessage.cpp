//
// Created by Jakub Dubec on 18/11/2018.
//

#include "NewMessage.h"

NewMessage::NewMessage(char *data) {
	int *int_pointer;

	// Read frame
	// TODO: runtime exception ak zly contex
	this->frame = data[0];

	// Read type
	this->type = data[1];

	// Read fragments
	int_pointer = (int*) (data + 2);
	this->fragments = *int_pointer;
}

NewMessage::NewMessage(TransportType type, int fragments) {
	this->frame = FrameType::newMessageType;
	this->type = type;
	this->fragments = fragments;
}

char *NewMessage::toBinary() {
	char *data = (char*) malloc(this->size());
	int *intprt = (int *) (data + 2);

	data[0] = this->frame;
	data[1] = this->type;
	*intprt = this->fragments;

	return data;
}

size_t NewMessage::size() {
	return sizeof(char) + sizeof(char) + sizeof(int);
}

std::string NewMessage::toString() {
	char buffer[100];
	sprintf(buffer, "NewMessage(frame, type, fragments) {%d, %d, %d}", this->frame, this->type, this->fragments);
	return std::string(buffer);
}

int NewMessage::getFragments() {
	return this->fragments;
}

char NewMessage::getTransportType() {
	return TransportType (this->type);
}
