#include <iostream>
#include <CLI/CLI.hpp>
#include "Mode/Server.h"
#include "Mode/Client.h"

#define SERVER "SERVER"
#define CLIENT "CLIENT"

#define MAX_FRAGMENT_SIZE 800

int main(int argc, char **argv) {
	CLI::App app{"UDP Client/Server communicator"};

	std::string mode;
	int fragmentSize = MAX_FRAGMENT_SIZE;
	int port = 0;
	int framesToDestroy = 0;
	int paritySkip = 0;
	std::string ip = "127.0.0.1";

	app.add_option("-m,--mode", mode, "Application mode (SERVER | CLIENT)")->required();
	app.add_option("-i, --ip", ip, "IP Address")->required();
	app.add_option("-p, --port", port, "Port")->required();
	app.add_option("-f,--fragment", fragmentSize, "Maximum fragment size")->check(CLI::Range(9, MAX_FRAGMENT_SIZE));
	app.add_option("-d,--destroy", framesToDestroy, "Number of frames to destroy");
	app.add_option("-s,--skip", paritySkip, "Parity skip")->check(CLI::Range(0, 1));

	CLI11_PARSE(app, argc, argv);

	if (mode == SERVER) {
		new Server(ip, port);
	}
	else if (mode == CLIENT) {
		new Client(ip, port, fragmentSize, framesToDestroy, paritySkip);
	}

	return 0;
}
